import sys, os
import matplotlib.pyplot as plt
import networkx as nx

class NetData:
    nodeNum = 0
    interestNum = 0
    linkMatrix = None
    interestMatrix = None

    diNodeNum = 0
    diNodeDict = {}
    diLinkMatrix = None

    def __init__(self, filename):
        fin = open(filename, "r")
        line = fin.readline()
        items = line.split(",")
        self.nodeNum = int(items[0])
        self.interestNum = int(items[1])
        self.linkMatrix = [[0.0 for col in range(self.nodeNum)] for row in range(self.nodeNum)]
        self.interestMatrix = [[0.0 for col in range(self.interestNum)] for row in range(self.nodeNum)]

        #read link matrix
        for i in range(self.nodeNum):
            line = fin.readline()
            items = line.split(",")
            print items
            for j in range(self.nodeNum):
                print items[j]
                self.linkMatrix[i][j] = float(items[j])

        #read interest matrix
        for i in range(self.nodeNum):
            line = fin.readline()
            items = line.split(",");
            print items
            for j in range(self.interestNum):
                self.interestMatrix[i][j] = float(items[j])

        #generate dinode

    def get_link_matrix(self):
        return self.linkMatrix

    def get_interest_matrix(self):
        return self.interestMatrix
                
    def get_node_num(self):
        return self.nodeNum

    def get_interest_num(self):
        return self.interestNum

class Visualizer:
    netData = None
    graph = None
    nodeColor = None
    nodeSize = None
    edgeColor = None
    threshold = 0

    defaultNodeSize = 100
    defaultNodeColor = 'blue'
    defaultEdgeWidth = 1

    dislikeColor = '#EE4000'
    likeColor =  '#008B8B'

    interestColor = ['#FF0000', '#00FF00', '#0000FF']

    def __init__(self, filename):
        self.netData = NetData(filename)
        if self.netData.get_interest_num() > 3:
            print "This Visualizer can only at most express 3 different interests"
            return
#        self.graph = nx.DiGraph()
        self.graph = nx.Graph()
        self.edgeColor = {}
        
        #add nodes to the graph
#        for i in range(self.netData.get_node_num()):
#            self.graph.add_node(i)

        self.nodeColor = [self.defaultNodeColor for i in range(self.netData.get_node_num())]
        self.nodeSize = [self.defaultNodeSize  for i in range(self.netData.get_node_num())]

        print self.netData.get_link_matrix()
        print self.netData.get_interest_matrix()

    def hex_to_rgb(self, value):
        value = value.lstrip('#')
        lv = len(value)
        return tuple(int(value[i:i+lv/3], 16) for i in range(0, lv, lv/3))

    def rgb_to_hex(self, rgb):
        return '#%02x%02x%02x' % rgb

    def get_edge_color(self, w):
        if w > 0:
            color = self.likeColor
        elif w < 0:
            color = self.dislikeColor
        else:
            return '#FFFFFF'

        rgbArr = [0 for i in range(3)]
        (rgbArr[0], rgbArr[1], rgbArr[2]) = self.hex_to_rgb(color)
        for i in range(3):
            tmp = 255 - rgbArr[i]
            rgbArr[i] = 255 - tmp * abs(w)

        return self.rgb_to_hex((rgbArr[0], rgbArr[1], rgbArr[2]))
        

    def set_node_color(self, nodeID, nodeColor):
        self.nodeColor[nodeID] = nodeColor

    def set_node_size(self, nodeID, nodeSize):
        self.nodeSize[nodeID] = nodeSize

    def draw(self):
        pos = nx.spring_layout(self.graph)
        nx.draw_networkx_nodes(self.graph, pos, node_color = self.nodeColor, node_size = self.nodeSize, with_labels=False)
        nx.draw_networkx_edges(self.graph, pos, edgelist=self.edgeColor.keys(), width = self.defaultEdgeWidth, edge_color=self.edgeColor.values())
        print 'position', 
        print pos
        plt.savefig("test.png")
        plt.show()

    def set_edge_threshold(self, th):
        self.threshold = th

    def refresh_edges(self):
        self.edgeColor = {}
        for i in range(self.netData.get_node_num()):
            for j in range(self.netData.get_node_num()):
                w = (self.netData.get_link_matrix())[i][j]
                if abs(w) >self.threshold:
                    self.edgeColor[(i, j)] = self.get_edge_color(w)
                    

    def refresh_nodes(self):
        self.graph.remove_nodes_from(self.graph.nodes())
        #add nodes to the graph
        for i in range(self.netData.get_node_num()):
            self.graph.add_node(i)

        rgbArr = [0 for i in range(3)]
        for i in range(self.netData.get_node_num()):
            for j in range(self.netData.get_interest_num()):
                rgbArr[j] = (1.0 + (self.netData.get_interest_matrix())[i][j]) * 255/2.0
            self.nodeColor[i] = self.rgb_to_hex((rgbArr[0], rgbArr[1], rgbArr[2]))
                

    def refresh_all(self):
        self.refresh_nodes()
        self.refresh_edges()

def main():
    filename = sys.argv[1]
    print filename  

    v = Visualizer(filename) 
    #v.set_node_color(1, '#A0CBE2')
    #v.set_node_size(1, 100)
    #v.set_node_color(2, '#B70733')
    #v.set_node_size(2, 200)
    v.refresh_all()
    v.draw()

if __name__=="__main__":
    main()














