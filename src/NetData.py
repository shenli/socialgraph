import sys, os

class NetData:
    nodeNum = 0
    interestNum = 0
    linkMatrix = None
    interestMatrix = None

    leaderNum = 0
    leaders = None

    diNodeNum = 0
    diNodeDict = {}
    diLinkMatrix = None

    #threshold of existence of one edge
    threshold = 0

    def __init__(self, filename, threshold = 0):
        self.threshold = threshold
        self.diNodeNum = 0
        self.leaders = []
        fin = open(filename, "r")
        line = fin.readline()
        items = line.split(",")
        self.nodeNum = int(items[0])
        self.interestNum = int(items[1])
        self.leaderNum = int(items[2])
        self.linkMatrix = [[0.0 for col in range(self.nodeNum)] for row in range(self.nodeNum)]
        self.interestMatrix = [[0.0 for col in range(self.interestNum)] for row in range(self.nodeNum)]

        #read leader ids
        line = fin.readline()
        items = line.split(",")
        for i in range(self.leaderNum):
            self.leaders.append(int(items[i]))

        #read link matrix
        for i in range(self.nodeNum):
            line = fin.readline()
            items = line.split(",")
            #print items
            for j in range(self.nodeNum):
                #print items[j]
                self.linkMatrix[i][j] = float(items[j])

        #read interest matrix
        for i in range(self.nodeNum):
            line = fin.readline()
            items = line.split(",");
            #print items
            for j in range(self.interestNum):
                self.interestMatrix[i][j] = float(items[j])

        self.refresh_dinode(self.threshold)


    def refresh_dinode(self, threshold):
        self.diNodeNum = 0
        #generate diNode
        for i in range(self.nodeNum):
            for j in range(i + 1, self.nodeNum):
                if abs(self.linkMatrix[i][j]) > threshold and abs(self.linkMatrix[j][i]) > threshold:
                    self.diNodeDict[(i, j)] = self.nodeNum + self.diNodeNum
                    self.diNodeNum = self.diNodeNum + 1



    def get_link_matrix(self):
        return self.linkMatrix

    def get_interest_matrix(self):
        return self.interestMatrix
                
    def get_node_num(self):
        return self.nodeNum

    def get_interest_num(self):
        return self.interestNum

    def get_dinode_num(self):
        return self.diNodeNum

    def get_dinode_dict(self):
        return self.diNodeDict

    def get_leader_num(self):
        return self.leaderNum

    def get_leader_list(self):
        return self.leaders


def main():
    filename = sys.argv[1]
    print filename 

    netData = NetData(filename)
    #print netData.get_dinode_dict()
    #print netData.get_node_num()
    #print netData.get_leader_list()
    #print netData.get_leader_num()

if __name__=="__main__":
    main()














