import sys, os, random
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import networkx as nx

from NetData import NetData

class Visualizer:
    netData = None
    graph = None
    nodeColor = None
    nodeSize = None
    edgeColor = None
    diEdgeColor = None
    threshold = 0
    pos = None

    defaultNodeSize = 20
    defaultLeaderSize = 40
    defaultNodeColor = 'blue'
    defaultEdgeWidth = 0.5


    edgeWeightPower = 1
    attractThreshold = 280
    iterations = 100
    diffScale = 0.5
    minNodeDist = 80

    likeColor = '#0020FF'
    dislikeColor =  '#FF8844'
    interestColor = ['#FF0000', '#00FF00', '#0000FF']

    edgeBarSpace = 0.15
    edgeBarWidth = 0.1
    edgeBarLength = 1
    edgeBarColor = [likeColor, '#FFFFFF', dislikeColor]
    edgeBarStepNum = 20 #steps between two successive edgeBarColor
    edgeBarNodeList = None

    def __init__(self, filename):
        self.netData = NetData(filename)
        if self.netData.get_interest_num() > 3:
            print "This Visualizer can only at most express 3 different interests"
            return
#        self.graph = nx.DiGraph()
        self.graph = nx.Graph()
        self.edgeColor = {}
        
        #add nodes to the graph
#        for i in range(self.netData.get_node_num()):
#            self.graph.add_node(i)

        self.nodeColor = [self.defaultNodeColor for i in range(self.netData.get_node_num())]
        self.nodeSize = [self.defaultNodeSize  for i in range(self.netData.get_node_num())]

        #print self.netData.get_link_matrix()
        #print self.netData.get_interest_matrix()

    def hex_to_rgb(self, value):
        value = value.lstrip('#')
        lv = len(value)
        return tuple(int(value[i:i+lv/3], 16) for i in range(0, lv, lv/3))

    def rgb_to_hex(self, rgb):
        return '#%02x%02x%02x' % rgb

    def get_edge_color(self, w):
        if w > 0:
            color = self.likeColor
        elif w < 0:
            color = self.dislikeColor
        else:
            return '#FFFFFF'

        rgbArr = [0 for i in range(3)]
        (rgbArr[0], rgbArr[1], rgbArr[2]) = self.hex_to_rgb(color)
        for i in range(3):
            tmp = 255 - rgbArr[i]
            rgbArr[i] = 255 - tmp * abs(w)

        return self.rgb_to_hex((rgbArr[0], rgbArr[1], rgbArr[2]))
        

    def set_node_color(self, nodeID, nodeColor):
        self.nodeColor[nodeID] = nodeColor

    def set_node_size(self, nodeID, nodeSize):
        self.nodeSize[nodeID] = nodeSize

    def _get_leader_color(self):
        colors = {}
        leaderList = self.netData.get_leader_list()
        for i in leaderList:
            colors[i] = self.nodeColor[i]
        return colors

    def _color_diff_func(self, dr, dg, db):
        return ((abs(dr) + abs(dg) + abs(db) - self.attractThreshold) * self.diffScale) ** self.edgeWeightPower

    def _get_color_diff(self, x, y):
        (xr, xg, xb) = self.hex_to_rgb(x)
        (yr, yg, yb) = self.hex_to_rgb(y)
        return self._color_diff_func(xr - yr, xg - yg, xb - yb)

    def _get_step_color(self, x, y, num, index):
        (xr, xg, xb) = self.hex_to_rgb(x)
        (yr, yg, yb) = self.hex_to_rgb(y)
        rStep = (yr - xr) / float(num)
        gStep = (yg - xg) / float(num)
        bStep = (yb - xb) / float(num)

        return self.rgb_to_hex((xr + rStep * index, xg + gStep * index, xb + bStep * index))

    def draw_color_bar(self, pos, colorRange, ticks):
        (minX, minY) = pos[0]
        (maxX, maxY) = pos[0]
        print "%f, %f, %f, %f"%(minX, minY, maxX, maxY)

        for node in pos.keys():
            (tmpX, tmpY) = pos[node]
            if tmpX > maxX:
                maxX = tmpX
            elif tmpX < minX:
                minX = tmpX

            if tmpY > maxY:
                maxY = tmpY
            elif tmpY < minY:
                minY = tmpY

        print "%f, %f, %f, %f"%(minX, minY, maxX, maxY)

        edgeBarX = maxX + self.edgeBarSpace * (maxX -minX)
        edgeBarMinY = minY
        edgeBarMaxY = maxY
        edgeBarDelY = maxY - minY
        edgeBarMinNode = self.netData.get_node_num() + self.netData.get_dinode_num() + 1
        edgeBarNodeNum = (len(self.edgeBarColor) - 1) * self.edgeBarStepNum + 1

        stepY = (maxY - minY) / float(edgeBarNodeNum - 1)

        edgeBarColorList = []
        #calculate color for each step
        for i in range(len(self.edgeBarColor) - 1):
            startColor = self.edgeBarColor[i]
            endColor = self.edgeBarColor[i+1]
            for j in range(self.edgeBarStepNum):
                edgeBarColorList.append(self._get_step_color(startColor, endColor, self.edgeBarStepNum, j))

        #add nodes for edgeBar 
        for i in range(edgeBarNodeNum):
            nodeID = i + edgeBarMinNode
            self.graph.add_node(nodeID)
            self.pos[nodeID] = (edgeBarX, edgeBarMinY + i * stepY)

        #add edges for edgeBar
        edgeBarEdgeList = []
        for i in range(edgeBarNodeNum - 1):
            nodeID = i + edgeBarMinNode
            edgeBarEdgeList.append((nodeID, nodeID + 1))

        nx.draw_networkx_edges(self.graph, self.pos, edgelist = edgeBarEdgeList, width = 8, edge_color = edgeBarColorList)

        #add lables
        self.graph.add_node('like')
        self.graph.add_node('dislike')
        self.pos['like'] = (edgeBarX, edgeBarMinY - edgeBarDelY * 0.02)
        self.pos['dislike'] = (edgeBarX, edgeBarMaxY + edgeBarDelY * 0.02)

        labels = {}
        labels['like'] = 'Like'
        labels['dislike'] = 'Dislike'
        nx.draw_networkx_labels(self.graph, self.pos, labels, font_size = 16)

    def draw(self):
        pos = nx.spring_layout(self.graph)
        nx.draw_networkx_nodes(self.graph, pos, node_color = self.nodeColor, node_size = self.nodeSize, with_labels=False)
        nx.draw_networkx_edges(self.graph, pos, edgelist=self.edgeColor.keys(), width = self.defaultEdgeWidth, edge_color=self.edgeColor.values())
        #draw leader
        colors = self._get_leader_color()
        nx.draw_networkx_nodes(self.graph, pos, nodelist = colors.keys(), node_color = colors.values(), 
                                                node_size = self.defaultLeaderSize, with_labels = False)
        print 'position', 
        #print pos
        plt.savefig("test.png")
        plt.show()

    def draw_digraph(self):
        #fig = plt.figure()
        #curAX = fig.add_subplot(111)
        nx.draw_networkx_nodes(self.graph, self.pos, nodelist = range(self.netData.get_node_num()), 
                                                node_color = self.nodeColor, node_size = self.nodeSize, with_labels = False)
        nx.draw_networkx_nodes(self.graph, self.pos, nodelist = range(self.netData.get_node_num(), self.netData.get_dinode_num()), 
                                                node_size = 0, with_labels = False)
        nx.draw_networkx_edges(self.graph, self.pos, self.diEdgeColor.keys(), width = self.defaultEdgeWidth, edge_color=self.diEdgeColor.values())
        #draw leader
        colors = self._get_leader_color()
        nx.draw_networkx_nodes(self.graph, self.pos, nodelist = colors.keys(), node_color = colors.values(), 
                                                node_size = self.defaultLeaderSize, with_labels = False)

        self.draw_color_bar(self.pos, [self.likeColor, '#FFFFFF', self.dislikeColor], ('like', 'dislike'))
      
 
        plt.savefig("digraph.png")
        #imData = plt.imread("digraph.png")
        #plt.sci(leaderNodes) 
        #plt.colorbar() 
        #plt.sci(allEdges) 
        #plt.colorbar()
        #plt.show()
        
        #curAX = pylab.gca()
        #fig = plt.figure()
        #plt.clim(-4, 4)
        #curAX = fig.add_subplot(111)
        #cax = curAX.imshow(imData)
        #cbar = fig.colorbar(cax, ticks = [0, 0.3], shrink = 0.8)
        #print curAX
        #cax = curAX.get_images()
        #print cax
        #fig.colorbar(curAX.get_images(), ticks = ['like', 'dislike'])
        plt.show()
        

    def set_edge_threshold(self, th):
        self.threshold = th

    def _refresh_edges(self, di = False):
        self.edgeColor = {}
        self.diEdgeColor = {}
        self.graph.remove_edges_from(self.graph.edges())

        #undirected graph
        if not di:
            for i in range(self.netData.get_node_num()):
                for j in range(i + 1, self.netData.get_node_num()):
                    w = (self.netData.get_link_matrix())[i][j]
                    if abs(w) >self.threshold:
                        self.edgeColor[(i, j)] = self.get_edge_color(w)
        #directed graph: add one virtual node in the middle of two real nodes to separate the color of two edges
        else: 
            #add dinode
            #self.pos = nx.spring_layout(self.graph)
            #self.pos = nx.graphviz_layout(self.graph)
            #self.pos = nx.fruchterman_reingold_layout(self.graph)
            self.netData.refresh_dinode(self.threshold)
            for i in range(self.netData.get_dinode_num()):
                self.graph.add_node(i + self.netData.get_node_num())

            diNodeDict = self.netData.get_dinode_dict()

            #find the minimum edge weights
            minEdgeWeight = 0
            for i in range(self.netData.get_node_num()):
                for j in range(i + 1, self.netData.get_node_num()):
                    edgeWeight = self._get_color_diff(self.nodeColor[i], self.nodeColor[j])
                    if minEdgeWeight > edgeWeight:
                        minEdgeWeight = edgeWeight

            #add undirected edges
            for i in range(self.netData.get_node_num()):
                for j in range(i + 1, self.netData.get_node_num()):
                    wl = (self.netData.get_link_matrix())[i][j]
                    wr = (self.netData.get_link_matrix())[j][i]
                    #wr= (self.netData.get_link_matrix())[j][i]

                    #if abs(w) > self.threshold or abs(wr) > self.threshold:
                    edgeWeight = self._get_color_diff(self.nodeColor[i], self.nodeColor[j]) + abs(minEdgeWeight) + self.minNodeDist
                    
                    #self.graph.add_edge(i, j, {'weight':edgeWeight})
                    self.graph.add_edge(i, j, {'len':edgeWeight})

                    w = 0

                    if (i, j) not in diNodeDict:
                        if abs(wl) > self.threshold:
                            w = wl
                        elif abs(wr) > self.threshold:
                            w = wr
                        if abs(w) >self.threshold :
                            self.diEdgeColor[(i, j)] = self.get_edge_color(w)
                            self.diEdgeColor[(j, i)] = self.get_edge_color(w) 

            #self.pos = nx.spring_layout(self.graph, iterations = self.iterations)
            #self.pos = nx.spectral_layout(self.graph)
            self.pos = nx.graphviz_layout(self.graph)             
            #print nx.to_agraph(self.graph)   

            #calculate dinode position & diedge color
            for diEdge in diNodeDict.keys():
                diNode = diNodeDict[diEdge]
                (x, y) = diEdge
                #calculate dinode position
                posX = self.pos[x]
                posY = self.pos[y]
                posDi = []
                for i in range(len(posX)):
                    posDi.append((posX[i] + posY[i]) / 2.0) 
                self.pos[diNode] = posDi
                #calculate diEdge color
                wxy = (self.netData.get_link_matrix())[x][y]
                wyx = (self.netData.get_link_matrix())[y][x]
                self.diEdgeColor[(x, diNode)] = self.get_edge_color(wxy)
                self.diEdgeColor[(y, diNode)] = self.get_edge_color(wyx)              


    def _refresh_nodes(self):
        self.graph.remove_nodes_from(self.graph.nodes())
        #add nodes to the graph
        for i in range(self.netData.get_node_num()):
            self.graph.add_node(i)

        rgbArr = [0 for i in range(3)]
        for i in range(self.netData.get_node_num()):
            for j in range(self.netData.get_interest_num()):
                rgbArr[j] = (1.0 + (self.netData.get_interest_matrix())[i][j]) * 255/2.0
            self.nodeColor[i] = self.rgb_to_hex((rgbArr[0], rgbArr[1], rgbArr[2]))
            self.graph.add_node(i, fillcolor = self.nodeColor[i])
                

    def refresh_all(self, di = False):
        self._refresh_nodes()
        self._refresh_edges(di)

def main():
    filename = sys.argv[1]
    print filename  

    v = Visualizer(filename)
    v.set_edge_threshold(0.05) 
    #v.set_node_color(1, '#A0CBE2')
    #v.set_node_size(1, 100)
    #v.set_node_color(2, '#B70733')
    #v.set_node_size(2, 200)
    #v.refresh_all()
    #v.draw()
    v.refresh_all(True)
    v.draw_digraph()

if __name__=="__main__":
    main()














